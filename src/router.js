import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '',
            name: 'home',
            component: Home
        },
        {
            path: '/forum',
            name: 'forum',
            component: () => { return import ('./views/Forum.vue') },
            props: true
        },
        {
            path: '/login',
            name: 'login',
            component: () => { return import ('./views/Login.vue') },
            props: true,
        },
        {
            path: '/profil',
            name: 'profil',
            component: () => { return import ('./components/ProfilCard.vue') },
            props: true,
        },
        {
            path: '/signup',
            name: 'signup',
            component: () => { return import ('./views/SignUp.vue') },
            props: true
        },
        {
            /*path: '/quote:id',*/
            path: '/quote/:id',
            name: 'quote',
            component: () => { return import ('./views/Quote.vue') },
            props: true
        },
        {
            path: '/newtopic',
            name: 'newtopic',
            component: () => { return import ('./views/Newtopic.vue') }
        }
    ]
})