import axios from 'axios';

let baseURL = "http://localhost:8000/"

export const HTTP = axios.create(
    {
        baseURL: baseURL,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        }
    })
